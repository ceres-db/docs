## HoraeDB Documentation

Welcome to HoraeDB documentation!

This repository stores all the source files of [HoraeDB docs website](https://ceresdb.github.io/docs), the website is generated with [mdbook](https://rust-lang.github.io/mdBook/).

**All files are written in standard markdown.**

## Contributing

We very much welcome any contributions from the community to make HoraeDB documentation better.

You can

- Open an [issue](https://github.com/CeresDB/docs/issues) with any suggestions, or
- Submit a [pull request](https://github.com/CeresDB/docs/pulls) to fix errors.
